class Book
  # TODO: your code goes here!
  def initialize
    @title
  end

  def title
    @title
  end

  def title=(title)
    excludes = ['and', 'a', 'an', 'if', 'then', 'they', 'the', 'in', 'of']
    @title = title.downcase.split(' ')
    @title.each do |word|
      word.capitalize! if !excludes.include?(word)
    end
    @title[0].capitalize!
    @title = @title.join(' ')
  end
end
